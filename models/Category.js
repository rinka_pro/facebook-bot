var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Category Model
 * ==========
 */

var Category = new keystone.List('Category');

Category.add({
	name: { type: Types.Text, initial: true, required: true, index: true },
	image: { type: Types.Text, initial: true, required: true, index: false },
	en: { type: Types.Text, initial: true, required: true, index: true },
	ru: { type: Types.Text, initial: true, required: true, index: true },
	active: { type: Types.Boolean, initial: true, index: true}
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone', index: true }
});

// Provide access to Keystone
Category.schema.virtual('canAccessKeystone').get(function() {
	return this.isAdmin;
});


/**
 * Registration
 */

Category.defaultColumns = 'name, active, en, ru, image';
Category.register();
