var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Fact Model
 * ==========
 */

var Fact = new keystone.List('Fact');

Fact.add({
	category: { type: Types.Text, initial: true, required: true, index: true },
	image: { type: Types.Text, initial: true, required: true, index: false },
	en: { type: Types.Text, initial: true, required: true, index: true },
	ru: { type: Types.Text, initial: true, required: true, index: true },
	active: { type: Types.Boolean, initial: true, index: true}
}, 'Permissions', {
	isAdmin: { type: Boolean, label: 'Can access Keystone', index: true }
});

// Provide access to Keystone
Fact.schema.virtual('canAccessKeystone').get(function() {
	return this.isAdmin;
});


/**
 * Registration
 */

Fact.defaultColumns = 'category, active, en, ru, image';
Fact.register();
